import './styles/app.scss';

import './bootstrap';

import './lib/counterup/jquery.waypoints.min.js';
import './lib/counterup/jquery.counterup.js';
import './lib/easing/easing.min.js';

$('.navbar-toggler').on('click', function () {
	if (!$('#mainNav').hasClass('navbar-reduce')) {
		$('#mainNav').addClass('navbar-reduce');
	} else {
		$('#mainNav').removeClass('navbar-reduce');
	}
})

// Preloader
$(window).on('load', function () {
	if ($('#preloader').length) {
		$('#preloader').delay(100).fadeOut('slow', function () {
			$(this).remove();
		});
	}
});

// Back to top button
$(window).on("scroll", function () {
	if ($(this).scrollTop() > 100) {
		$('.back-to-top').fadeIn('slow');
	} else {
		$('.back-to-top').fadeOut('slow');
	}
});

$('.back-to-top').on("click", function () {
	$('html').animate({ scrollTop: 0 }, 1500, 'easeInOutExpo');
	return false;
});

/* Facts Counter */
$('.counter').counterUp({
	delay: 15,
	time: 2000
});

// Closes menu when a clicked
$('.js-scroll').on("click", function () {
	$('.navbar-collapse').collapse('hide');
});

// Change dropdown button text on select
$(".dropdown-menu a").on("click", function () {
	$(this).parents(".dropdown").find('.btn').html(`<strong>${$(this).text()}</strong>`);
});