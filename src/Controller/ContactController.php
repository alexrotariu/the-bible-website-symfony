<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(): Response
    {
        return $this->render('contact/index.html.twig', [
            'page' => 'contact',
        ]);
    }

    /**
     * @Route("/contact/send", name="send_message")
     */
    public function sendMessage(): RedirectResponse {

        $name = $_POST["name"];
        $email = $_POST["email"];
        $message = $_POST["message"];

        if (!empty($name) && !empty($email) && !empty($message)) {
            $this->saveMessageLocally($name, $email, $message);
            return $this->redirectToRoute('contact');
        } else {
            return $this->redirectToRoute('contact');
        }
    }

    public function saveMessageLocally(string $name, string $email, string $message)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $contactMessage = new ContactMessage();
        $contactMessage->setName($name);
        $contactMessage->setEmail($email);
        $contactMessage->setMessage($message);

        $entityManager->persist($contactMessage);

        $entityManager->flush();
    }
}