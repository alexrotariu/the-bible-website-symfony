<?php

namespace App\Controller;

use App\Utils\VerseRemote;
use App\Entity\Verse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ReadController extends AbstractController
{
    /**
     * @Route("/read/{book}/{chapter}", name="read")
     */
    public function index(string $book = "Genesis", string $chapter = "1"): Response
    {

        $reference_param = "$book+$chapter";
        $passage = $this->getChapterRemote($reference_param);

        $reference = $passage->reference;
        $versesResponse = $passage->verses;
        $verses = $this->mapVersesResponseToVersesRemote($versesResponse);

        return $this->render('read/index.html.twig', [
            'page' => 'read',
            'reference' => $reference,
            'verses' => $verses
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function search(): RedirectResponse {

        $book = str_replace(' ', '', $_GET["book"]);;
        $chapter = $_GET["chapter"];

        if (empty($chapter)) {
            if (empty($book)) {
                return $this->redirectToRoute('read');
            } else {
                return $this->redirectToRoute('read', ['book' => $book]);
            }
        } else {
            return $this->redirectToRoute('read', ['book' => $book, 'chapter' => $chapter]);
        }
    }

    /**
     * @Route("/save/{book}/{chapter}/{number}/{text}", name="save")
     */
    public function saveVerse(string $book, string $chapter, string $number, string $text): RedirectResponse {

        $this->saveVerseLocally($book, $chapter, $number, $text);
        return $this->redirectToRoute('read', ['book' => $book, 'chapter' => $chapter]);
    }

    /**
     * @Route("/unsave/{book}/{chapter}/{number}/{routeName}", name="unsave")
     */
    public function unsaveVerse(string $book, string $chapter, string $number, string $routeName): RedirectResponse {

        $this->deleteVerseLocally($book, $chapter, $number);
        if ($routeName == "read") {
            return $this->redirectToRoute('read', ['book' => $book, 'chapter' => $chapter]);
        } else {
            return $this->redirectToRoute('saved');
        }
    }
    
    /**
     * @Route("/saved", name="saved")
     */
    public function saved(): Response
    {
        $versesResponse = $this->getAllSavedVerses();
        $verses = array_values($versesResponse);

        return $this->render('read/saved.html.twig', [
            'page' => 'saved',
            'verses' => $verses
        ]);
    }

    public function mapVersesResponseToVersesRemote(array $versesResponse): array
    {
        $versesRemote = array();
        foreach ($versesResponse as $value) {
            $v = new VerseRemote();
            $v->setBook($value->book_name);
            $v->setChapter($value->chapter);
            $v->setNumber((int)($value->verse));
            $v->setText($value->text);
            $v->setSaved(false);
            foreach ($this->getAllSavedVerses() as $value) {
                $local_reference_ID = $value->getBook() . ":" . (string)($value->getChapter()) . ":" . (string)($value->getNumber());
                $remote_reference_ID = $v->getBook() . ":" . (string)($v->getChapter()) . ":" . (string)($v->getNumber());
                
                if ($remote_reference_ID == $local_reference_ID) {
                    $v->setSaved(true);
                    break;
                }
            }
            array_push($versesRemote, $v);
        }
        return $versesRemote;
    }

    public function getChapterRemote($reference)
    {
        $url = "https://bible-api.com/$reference";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch));
        curl_close($ch);
        return $output;
    }

    public function getAllSavedVerses()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Verse::class);
        $savedVerses = $repository->findAll();
        return $savedVerses;
    }

    public function saveVerseLocally(string $book, string $chapter, string $number, string $text)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $verse = new Verse();
        $verse->setBook($book);
        $verse->setChapter($chapter);
        $verse->setNumber((int)($number));
        $verse->setText($text);

        $entityManager->persist($verse);

        $entityManager->flush();
    }

    public function deleteVerseLocally(string $book, string $chapter, string $number)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getManager()->getRepository(Verse::class);

        $verse = $repository->findOneBy([
            'book' => $book,
            'chapter' => $chapter,
            'number' => $number,
        ]);

        $entityManager->remove($verse);
        $entityManager->flush();
    }
}
